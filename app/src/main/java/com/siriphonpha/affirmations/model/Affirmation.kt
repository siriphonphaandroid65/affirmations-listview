package com.siriphonpha.affirmations.model

import android.security.identity.AccessControlProfileId
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Affirmation(@StringRes val stringResurceId: Int, @DrawableRes val imageResourceId: Int)
