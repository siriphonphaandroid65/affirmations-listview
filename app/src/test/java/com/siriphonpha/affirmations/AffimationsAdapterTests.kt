package com.siriphonpha.affirmations

import android.content.Context
import com.siriphonpha.affirmations.adapter.ItemAdapter
import com.siriphonpha.affirmations.model.Affirmation
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito.mock

class AffimationsAdapterTests {
    private val  context = mock(Context::class.java)
    @Test
    fun  adapter_size() {
        val data = listOf(
            Affirmation(R.string.affirmation1, R.drawable.image1),
            Affirmation(R.string.affirmation2, R.drawable.image2)
        )
        val  adapter = ItemAdapter(context, data)
        assertEquals("ItemAdapter is not correct size ", data.size, adapter.itemCount)
    }
}